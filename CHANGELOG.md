# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- ## Unreleased -->

## [0.0.2](https://gitlab.com/mneumann_ntecs/xtra-addons/-/tree/v0.0.2) - 2021-01-25

**Features**

- Public export of `xtra::spawn::*` in addition to `xtra::prelude::*`.

- Added `Broker::<T>::subscribe` and `Broker::<T>::publish` methods.

## [0.0.1](https://gitlab.com/mneumann_ntecs/xtra-addons/-/tree/v0.0.1) - 2021-01-11

**Features**

- Initial realease.

