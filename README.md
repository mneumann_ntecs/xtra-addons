# xtra-addons

Add-on functionality (`Broker`, `Registry`) for [xtra][xtra], the tiny actor
framework (Rust).

[xtra]: https://crates.io/crates/xtra
