use crate::*;
use async_trait::async_trait;

#[async_trait]
pub trait FromRegistryExt: Actor {
    async fn from_registry() -> Address<Self>;
}

#[async_trait]
impl<A: Actor + Default> FromRegistryExt for A {
    async fn from_registry() -> Address<Self> {
        Registry::get_or_create_default::<Self>().await
    }
}

#[async_trait]
pub trait FromRegistryExtOpt: Actor {
    async fn from_registry_opt() -> Option<Address<Self>>;
}

#[async_trait]
impl<A: Actor> FromRegistryExtOpt for A {
    async fn from_registry_opt() -> Option<Address<Self>> {
        Registry::get::<Self>().await
    }
}

#[cfg(test)]
#[async_std::test]
async fn test_from_registry() {
    use FromRegistryExt;

    #[derive(Default)]
    struct A;
    impl Actor for A {}

    let _: Address<A> = A::from_registry().await;
    assert!(true);
}
