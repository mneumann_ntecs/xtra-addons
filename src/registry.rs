use futures::lock::Mutex;
use once_cell::sync::OnceCell;
use std::any::{Any, TypeId};
use std::collections::HashMap;
use xtra::prelude::*;
use xtra::spawn::AsyncStd;

static GLOBAL_REGISTRY: OnceCell<Mutex<HashMap<TypeId, Box<dyn Any + Send>>>> = OnceCell::new();

pub struct Registry;

impl Registry {
    pub async fn get<A: Actor>() -> Option<Address<A>> {
        let registry = GLOBAL_REGISTRY.get_or_init(Default::default);
        let registry = registry.lock().await;

        registry
            .get(&TypeId::of::<A>())
            .map(|addr| addr.downcast_ref::<Address<A>>().unwrap().clone())
    }

    pub async fn get_or_create_default<A: Actor + Default>() -> Address<A> {
        let registry = GLOBAL_REGISTRY.get_or_init(Default::default);
        let mut registry = registry.lock().await;

        match registry.get_mut(&TypeId::of::<A>()) {
            Some(addr) => addr.downcast_ref::<Address<A>>().unwrap().clone(),
            None => {
                let actor_manager = A::default().create(None);
                let addr = actor_manager.spawn(&mut AsyncStd);
                registry.insert(TypeId::of::<Self>(), Box::new(addr.clone()));
                addr
            }
        }
    }

    pub async fn register<A: Actor>(addr: Address<A>) -> Result<(), &'static str> {
        let registry = GLOBAL_REGISTRY.get_or_init(Default::default);
        let mut registry = registry.lock().await;

        match registry.get_mut(&TypeId::of::<A>()) {
            Some(_) => Err("Duplicate entry"),
            None => {
                registry.insert(TypeId::of::<A>(), Box::new(addr));
                Ok(())
            }
        }
    }
}
